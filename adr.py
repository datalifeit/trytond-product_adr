# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelSQL, ModelView, fields
from trytond.modules.company import CompanyReport
from trytond.wizard import Wizard, Button, StateReport, StateView
from trytond.pool import Pool
from functools import partial
from itertools import groupby

CATEGORIES = 5


class TransportCategory(ModelView, ModelSQL):
    """Transport Category"""
    __name__ = 'product.adr.transport_category'

    code = fields.Char('Code', required=True)
    name = fields.Char('Name', required=True)
    package_multiple = fields.Integer('Package Multiple', required=True)

    @staticmethod
    def default_package_multiple():
        return 1


class Adr(ModelView, ModelSQL):
    """Adr"""
    __name__ = 'product.adr'

    type = fields.Char('Type', required=True)
    code = fields.Char('Code', required=True)
    name = fields.Char('Name', required=True)
    adr_class = fields.Selection([
        ('1', '1'),
        ('2', '2'),
        ('3', '3'),
        ('4.1', '4.1'),
        ('4.2', '4.2'),
        ('4.3', '4.3'),
        ('5.1', '5.1'),
        ('5.2', '5.2'),
        ('6.1', '6.1'),
        ('6.2', '6.2'),
        ('7', '7'),
        ('8', '8'),
        ('9', '9'),
        ('', '')], 'Class', translate=False)
    warning_label = fields.Char('Warning Label')
    packaging_group = fields.Selection([
        ('I', 'I'),
        ('II', 'II'),
        ('III', 'III'),
        ('', '')], 'Packaging Group', translate=False)
    tunnel_code = fields.Selection([
        ('(A)', '(A)'),
        ('(B)', '(B)'),
        ('(C)', '(C)'),
        ('(D)', '(D)'),
        ('(D/E)', '(D/E)'),
        ('(E)', '(E)'),
        ('', '')], 'Tunnel Code', translate=False)
    transport_category = fields.Many2One('product.adr.transport_category',
        'Transport Category')

    @classmethod
    def default_type(cls):
        return 'UN'


class ShipmentWaybillReportMixin(CompanyReport):
    _shipment_model_name = ''

    @classmethod
    def get_context(cls, records, header, data):
        pool = Pool()
        LoadOrder = pool.get('carrier.load.order')
        report_context = super().get_context(records, header, data)

        orders = LoadOrder.search([
            ('shipment.id', 'in', [r.id for r in records],
                cls._shipment_model_name)])
        loads = list(set(order.load for order in orders))
        report_context['loads'] = loads

        adr_totals = {x: 0 for x in range(CATEGORIES)}
        shipment_moves = cls._get_moves(loads)
        adr_moves = [move for move in shipment_moves
            if move.product.adr]
        for i in range(CATEGORIES):
            category_totals = [x for x in adr_moves
                if x.product.adr.transport_category and
                    x.product.adr.transport_category.name == str(i)]
            adr_totals[i] = (sum(move.quantity * move.product.adr_multiple()
                for move in category_totals), category_totals[0].uom.symbol if
                    category_totals else '')
        report_context['adr_totals'] = adr_totals
        report_context['total_moves'] = (lambda record:
            cls.get_total_moves(record))
        report_context['shipment_moves'] = (lambda record:
            cls._get_shipment_moves(record))
        return report_context

    @classmethod
    def _get_shipment_moves(cls, shipment):
        return []

    @classmethod
    def _get_moves(cls, loads):
        return []

    @classmethod
    def get_total_moves(cls, record):
        return []


class ShipmentOutWaybill(ShipmentWaybillReportMixin):
    """Shipment Waybill"""
    __name__ = 'stock.shipment.out.waybill'
    _shipment_model_name = 'stock.shipment.out'

    @classmethod
    def _get_shipment_moves(cls, shipment):
        return shipment.outgoing_moves

    @classmethod
    def _get_moves(cls, loads):
        return [move for load in loads for order in load.orders for
                move in order.shipment.outgoing_moves]

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)

        loads_collection = []
        for load in report_context['loads']:
            delivery_addresses = set(order.shipment.delivery_address for
                order in load.orders)
            addresses_collection = []
            for address in delivery_addresses:
                shipments = [y.shipment for y in [x for x in load.orders
                    if x.shipment.delivery_address == address]]
                addresses_collection.append((address, shipments))
            loads_collection.append((load, addresses_collection))
        report_context['loads'] = loads_collection

        return report_context

    @classmethod
    def get_total_moves(cls, record):
        return [move for address_tuple in record
            for shipment in address_tuple[1]
            for move in shipment.outgoing_moves]


class ShipmentOutReturnWaybill(ShipmentWaybillReportMixin):
    """Shipment Waybill"""
    __name__ = 'stock.shipment.out.return.waybill'
    _shipment_model_name = 'stock.shipment.out.return'

    @classmethod
    def _get_shipment_moves(cls, shipment):
        return shipment.incoming_moves

    @classmethod
    def _get_moves(cls, loads):
        return [move for load in loads for order in load.orders for
                move in order.shipment.incoming_moves]

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)

        loads_collection = []
        for load in report_context['loads']:
            delivery_addresses = set(order.shipment.delivery_address for
                order in load.orders)
            addresses_collection = []
            for address in delivery_addresses:
                shipments = [y.shipment for y in [x for x in load.orders
                    if x.shipment.delivery_address == address]]
                addresses_collection.append((address, shipments))
            loads_collection.append((load, addresses_collection))
        report_context['loads'] = loads_collection

        return report_context

    @classmethod
    def get_total_moves(cls, record):
        return [move for address_tuple in record
            for shipment in address_tuple[1]
            for move in shipment.incoming_moves]


class ShipmentInternalWaybill(ShipmentWaybillReportMixin):
    """Shipment Waybill"""
    __name__ = 'stock.shipment.internal.waybill'
    _shipment_model_name = 'stock.shipment.internal'

    @classmethod
    def _get_shipment_moves(cls, shipment):
        return shipment.moves

    @classmethod
    def _get_moves(cls, loads):
        return [move for load in loads for order in load.orders for
                move in order.shipment.moves]

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)

        loads_collection = []
        for load in report_context['loads']:
            locations = set([order.shipment.to_location
                for order in load.orders])
            whs_per_loc = {l.id: cls._get_location_warehouse(l)
                for l in locations}
            whs = set([cls._get_location_warehouse(l) for l in locations])

            delivery_addresses = set(wh.address for wh in whs if wh)
            addresses_collection = []
            for address in delivery_addresses:
                shipments = [o.shipment for o in load.orders
                    if whs_per_loc[o.shipment.to_location.id] and
                    whs_per_loc[o.shipment.to_location.id].address == address]
                addresses_collection.append((address, shipments))
            loads_collection.append((load, addresses_collection))
        report_context['loads'] = loads_collection

        return report_context

    @classmethod
    def _get_location_warehouse(cls, location):
        res = None
        if location.type not in ('storage', 'view'):
            return res
        curr_loc = location
        while True:
            if not curr_loc.parent:
                break
            if curr_loc.parent.type == 'warehouse':
                res = location.parent
                break
            curr_loc = curr_loc.parent
        return res

    @classmethod
    def get_total_moves(cls, record):
        return [move for address_tuple in record
            for shipment in address_tuple[1]
            for move in shipment.moves]


class ShipmentInReturnWaybill(ShipmentWaybillReportMixin):
    """Shipment In return Waybill"""
    __name__ = 'stock.shipment.in.return.waybill'
    _shipment_model_name = 'stock.shipment.in.return'

    @classmethod
    def _get_shipment_moves(cls, shipment):
        return shipment.moves

    @classmethod
    def _get_moves(cls, loads):
        return [move for load in loads for order in load.orders for
                move in order.shipment.moves]

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)

        loads_collection = []
        for load in report_context['loads']:
            delivery_addresses = set(order.shipment.delivery_address for
                order in load.orders)
            addresses_collection = []
            for address in delivery_addresses:
                shipments = [y.shipment for y in [x for x in load.orders
                    if x.shipment.delivery_address == address]]
                addresses_collection.append((address, shipments))
            loads_collection.append((load, addresses_collection))
        report_context['loads'] = loads_collection

        return report_context

    @classmethod
    def get_total_moves(cls, record):
        return [move for address_tuple in record
            for shipment in address_tuple[1]
            for move in shipment.moves]


class ProductAdrAnualReportPrint(Wizard):
    '''Carrier Load Adr Anual Report Print'''
    __name__ = 'product.adr.anual_report.print'

    start = StateView(
        'product.adr.anual_report.print.start',
        'product_adr.product_adr_anual_report_print_start_view_form',
        [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-ok', default=True),
        ])
    print_ = StateReport('product.adr.anual_report')

    def do_print_(self, action):
        return action, {
            'start_date': self.start.start_date,
            'end_date': self.start.end_date,
            'warehouses': [w.id for w in self.start.warehouses]

        }


class ProductAdrAnualReportPrintStart(ModelSQL, ModelView):
    '''Carrier Load Adr Anual Report Start'''
    __name__ = 'product.adr.anual_report.print.start'

    start_date = fields.Date('Start Date', required=True)
    end_date = fields.Date('End Date', required=True)
    warehouses = fields.One2Many('stock.location', None,
        'Warehouses', domain=[('type', '=', 'warehouse')])


class ProductAdrAnualReport(CompanyReport):
    '''Carrier Load Anual Report'''
    __name__ = 'product.adr.anual_report'

    @classmethod
    def get_context(cls, records, header, data):
        pool = Pool()
        ShipmentIn = pool.get('stock.shipment.in')
        ShipmentOut = pool.get('stock.shipment.out')
        Uom = pool.get('product.uom')

        report_context = super().get_context(records, header, data)
        _domain = [
            ('effective_date', '>=', data['start_date']),
            ('effective_date', '<=', data['end_date']),
            ('state', '=', 'done')
        ]
        if data['warehouses']:
            _domain.append(('warehouse', 'in', data['warehouses']))

        uoms = cls._get_uoms()
        report_context['inventory_moves'] = []
        report_context['outgoing_moves'] = []

        shipments_in = ShipmentIn.search(_domain)
        moves = [move for shipment in shipments_in for move in
            shipment.inventory_moves if move.product.adr]
        keyfunc = partial(cls._get_group_move_key, moves)
        sorted_moves = sorted(moves, key=keyfunc)
        for keys, moves in groupby(sorted_moves, key=keyfunc):
            grouped_moves = list(moves)
            uom = Uom(uoms[keys[1].id])
            grouped_quantity = sum(Uom.compute_qty(m.uom,
                m.quantity, uom) for m in grouped_moves)
            grouped_move = grouped_moves[0]
            keys = keys[0:1] + keys[2:]
            if any(keys) and grouped_move.product.adr:
                lines = cls._get_moves(grouped_move, type_='IN')
                for line in lines:
                    line.update({
                        'quantity': grouped_quantity,
                        'uom': uom
                    })
                report_context['inventory_moves'].extend(lines)

        _domain.append(('load', '!=', None))
        shipments_out = ShipmentOut.search(_domain)
        moves = [move for shipment in shipments_out for move in
            shipment.outgoing_moves if move.product.adr]
        keyfunc = partial(cls._get_group_move_key, moves)
        sorted_moves = sorted(moves, key=keyfunc)
        moves_ = []
        for keys, moves in groupby(sorted_moves, key=keyfunc):
            grouped_moves = list(moves)
            uom = Uom(uoms[keys[1].id])
            grouped_quantity = sum(Uom.compute_qty(m.uom,
                m.quantity, uom) for m in grouped_moves)
            grouped_move = grouped_moves[0]
            keys = keys[0:1] + keys[2:]
            if any(keys) and grouped_move.product.adr:
                lines = cls._get_moves(grouped_move,
                    type_=cls._check_party_relation_company(
                        grouped_move.shipment.load) if
                        grouped_move.shipment.load else 'OUT')
                for line in lines:
                    line.update({
                        'quantity': grouped_quantity,
                        'uom': uom
                    })

                moves_.extend(lines)

        report_context['outgoing_moves'] = sorted(moves_,
            key=lambda x: x['type'])
        return report_context

    @classmethod
    def _get_moves(cls, move, type_):
        res = [{
            'type': type_,
            'adr_class': move.product.adr.adr_class,
            'packaging_group': move.product.adr.packaging_group,
            'warning_label': move.product.adr.warning_label,
            'zip': move.shipment.warehouse.address and
                move.shipment.warehouse.address.zip or '',
            'address': move.shipment.warehouse.address
        }]
        if type_ in ('OUT', 'TRANSPORT'):
            res[0]['address'] = move.shipment.delivery_address
        if res[0]['type'] == 'TRANSPORT':
            res.append(res[0].copy())
            res[1]['type'] = 'OUT'
            res[0]['zip'] = ''
            res[0]['address'] = None
        return res

    @classmethod
    def _check_party_relation_company(cls, load):
        Modeldata = Pool().get('ir.model.data')
        company_rel_id = Modeldata.get_id(
            'product_adr', 'party_relation_carrier_of')

        for rel in load.carrier.party.relations:
            if (rel.type.id == company_rel_id and
                    rel.to.id == load.company.party.id):
                return 'TRANSPORT'
        return 'OUT'

    @classmethod
    def _get_uoms(cls):
        pool = Pool()
        Modeldata = pool.get('ir.model.data')

        return {
            Modeldata.get_id('product', 'uom_cat_weight'):
                Modeldata.get_id('product', 'uom_kilogram'),
            Modeldata.get_id('product', 'uom_cat_volume'):
                Modeldata.get_id('product', 'uom_liter'),
            Modeldata.get_id('product', 'uom_cat_unit'):
                Modeldata.get_id('product', 'uom_unit')
        }

    @classmethod
    def _get_group_move_key(cls, moves, move):
        return (move.product.adr.adr_class or '',
            move.product.default_uom_category,
            move.product.adr.packaging_group or '',
            move.product.adr.warning_label or '',
            move.shipment.warehouse.address.street if
                move.shipment.warehouse.address else '')
