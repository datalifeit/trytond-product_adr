# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from .adr import (Adr, TransportCategory, ShipmentOutWaybill,
    ShipmentInternalWaybill, ProductAdrAnualReportPrint,
    ProductAdrAnualReportPrintStart, ProductAdrAnualReport,
    ShipmentOutReturnWaybill, ShipmentInReturnWaybill)
from .product import Product, Package, Move


def register():
    Pool.register(
        TransportCategory,
        Adr,
        Package,
        Product,
        Move,
        ProductAdrAnualReportPrintStart,
        module='product_adr', type_='model')
    Pool.register(
        ShipmentOutWaybill,
        ShipmentInternalWaybill,
        ShipmentOutReturnWaybill,
        ShipmentInReturnWaybill,
        ProductAdrAnualReport,
        module='product_adr', type_='report')
    Pool.register(
        ProductAdrAnualReportPrint,
        module='product_adr', type_='wizard')
